import numpy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import LinearSVC
import itertools

class BaseCaseClassifier(object):
  def __init__(self):
    self.vectorizer = CountVectorizer()
    self.classifier_class = MultinomialNB
    self.classifier_params = {}

  def train(self, city_names, city_codes, country_codes):
    X = self.vectorizer.fit_transform(city_names)

    y1 = numpy.array(city_codes)
    y2 = numpy.array(country_codes)

    self.city_classifier = self.classifier_class(**self.classifier_params)
    self.city_classifier.fit(X, y1)

    self.country_classifier = self.classifier_class(**self.classifier_params)
    self.country_classifier.fit(X, y2)

  def predict(self, city_name):
    predictor_vector = self.vectorizer.transform([city_name])
    predicted_city_code = self.city_classifier.predict(predictor_vector)[0]
    predicted_country_code = self.country_classifier.predict(predictor_vector)[0]
    return (predicted_city_code, predicted_country_code)

class NCharClassifier(BaseCaseClassifier):
    def __init__(self, **kwargs):
      ngram_range = kwargs['ngram_range']
      self.vectorizer = CountVectorizer(analyzer='char_wb', ngram_range=ngram_range)
      self.classifier_class = MultinomialNB

class LinearSVMClassifier(BaseCaseClassifier):  
  def __init__(self):
    self.vectorizer = CountVectorizer()
    self.classifier_class = LinearSVC
    self.classifier_params = {}

class LinearSVMWeightedClassifier(BaseCaseClassifier):
  def __init__(self, **kwargs):
    self.vectorizer = CountVectorizer(analyzer='char_wb', ngram_range=(2,2))
    self.classifier_class = LinearSVC
    self.classifier_params = {'C':kwargs['C']}

class CascadeNCharClassifier(object):
  def __init__(self, country_class_prob_threshold = 0.95):
    self.country_class_prob_threshold = country_class_prob_threshold

  def train(self, city_names, city_codes, country_codes):
    self.vectorizer = CountVectorizer(analyzer='char_wb', ngram_range=(2,2))
    X = self.vectorizer.fit_transform(city_names)

    self.country_classifier = MultinomialNB()
    y1 = numpy.array(country_codes)
    self.country_classifier.fit(X, y1)

    # country_code -> list of indices of X
    country_code_indices = {}

    for index, country_code in enumerate(country_codes):
      if country_code not in country_code_indices:
        country_code_indices[country_code] = [index]
      else:
        country_code_indices[country_code].append(index)

    y2 = numpy.array(city_codes)

    # independent city code classifier
    self.independent_city_code_classifier = MultinomialNB()
    self.independent_city_code_classifier.fit(X, y2)

    # country_code -> NB classifier
    self.city_code_classifiers = {}

    for country_code in country_code_indices.keys():
      self.city_code_classifiers[country_code] = MultinomialNB()
      country_indices = country_code_indices[country_code]
      self.city_code_classifiers[country_code].fit(X[country_indices, :], y2[country_indices])

  def predict(self, city_name):
    predictor_vector = self.vectorizer.transform([city_name])
    predicted_country_code = self.country_classifier.predict(predictor_vector)[0]
    predicted_country_code_prob = max(self.country_classifier.predict_proba(predictor_vector)[0])
    if predicted_country_code_prob > self.country_class_prob_threshold:
      # we're sure in country classification, using specific classifier
      chosen_city_classifier = self.city_code_classifiers[str(predicted_country_code)]
      predicted_city_code = chosen_city_classifier.predict(predictor_vector)[0]
    else:
      predicted_city_code = self.independent_city_code_classifier.predict(predictor_vector)[0]
    return (predicted_city_code, predicted_country_code)
