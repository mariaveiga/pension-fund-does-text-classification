from classy import BaseCaseClassifier, NCharClassifier, LinearSVMWeightedClassifier, CascadeNCharClassifier

import csv
import numpy
from sklearn import cross_validation

city_codes, country_codes, city_names = [], [], []

with open('handout/training.csv', 'rb') as training_file:
  reader = csv.reader(training_file, delimiter=',')
  for city_name, city_code, country_code in reader:
    city_codes.append(city_code)
    country_codes.append(country_code)
    city_names.append(city_name)

city_codes, country_codes, city_names = numpy.array(city_codes), numpy.array(country_codes), numpy.array(city_names)

'''
>>> pred = (123456, 999)
>>> real = (123444, 999)
>>> get_cost(pred, real)
1.0
'''
def get_cost(predicted_tuple, real_tuple):
  city_fail, country_fail = map(lambda (predicted, real): predicted != real, zip(predicted_tuple, real_tuple))
  return 1 * city_fail + 0.25 * country_fail

class CountPrinter:
  def __init__(self, total):
    self.total = total
    self.current = 0

  def count(self):
    print '%s/%s' % (self.current, self.total)
    self.current += 1

''' All the keyword arguments are passed to classifier initializer 
'''
def cv(classifier_class, n_folds = 10, verbose=False, **kwargs):
  sum_cost = 0
  if verbose: counter = CountPrinter(n_folds)
  folds = cross_validation.KFold(len(city_codes), n_folds=n_folds)
  for train_indices, test_indices in folds:
    if verbose: counter.count()
    classifier = classifier_class(**kwargs)
    classifier.train(city_names[train_indices], city_codes[train_indices], country_codes[train_indices])
    for test_index in test_indices:
      predicted = classifier.predict(city_names[test_index])
      sum_cost += get_cost(predicted, (city_codes[test_index], country_codes[test_index]))
  
  return sum_cost

print cv(LinearSVMWeightedClassifier, n_folds=300, verbose=False, C=0.02)
