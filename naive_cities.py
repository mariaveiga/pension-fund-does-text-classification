''' I generate classifications for validation set, run me like:
naive_cities.py > pension3.csv
'''
import csv
import numpy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB

city_codes = []
country_codes = []
city_names = []

with open('handout/training.csv', 'rb') as training_file:
  reader = csv.reader(training_file, delimiter=',')
  for city_name, city_code, country_code in reader:
    city_codes.append(city_code)
    country_codes.append(country_code)
    city_names.append(city_name.lower())

vectorizer = CountVectorizer()
X = vectorizer.fit_transform(city_names)

y1 = numpy.array(city_codes)
y2 = numpy.array(country_codes)

city_classifier = MultinomialNB()
city_classifier.fit(X, y1)

country_classifier = MultinomialNB()
country_classifier.fit(X, y2)

with open('handout/validation.csv', 'rb') as data:
  for city_name in data:
    to_classify = city_name[:-1].lower()
    predictor_vector = vectorizer.transform([to_classify])
    predicted_city_code = city_classifier.predict(predictor_vector)[0]
    predicted_country_code = country_classifier.predict(predictor_vector)[0]
    print "%s,%s" % (predicted_city_code, predicted_country_code)
