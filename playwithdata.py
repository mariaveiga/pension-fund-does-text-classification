import csv
import json

cities = {}

with open('handout/training.csv', 'rb') as training_file:
  reader = csv.reader(training_file, delimiter=',')
  for city_name, city_code, country_code in reader:
    if city_code in cities:
      cities[city_code].append(city_name.lower())
    else:
      cities[city_code] = [city_name.lower()]

print json.dumps(cities, indent=2)