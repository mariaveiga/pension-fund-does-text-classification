from classy import LinearSVMWeightedClassifier
classifier = LinearSVMWeightedClassifier(C=0.02)

import csv

TO_TRAIN_ON = 'handout/training.csv'
TO_CLASSIFY = 'handout/validation.csv'

city_codes = []
country_codes = []
city_names = []

with open(TO_TRAIN_ON, 'rb') as training_file:
  reader = csv.reader(training_file, delimiter=',')
  for city_name, city_code, country_code in reader:
    city_codes.append(city_code)
    country_codes.append(country_code)
    city_names.append(city_name.lower())

classifier.train(city_names, city_codes, country_codes)

with open(TO_CLASSIFY, 'rb') as data:
  for city_name in data:
    to_classify = city_name[:-1].lower()
    print "%s,%s" % classifier.predict(to_classify)
